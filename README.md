Charm/Cli
=========

An extendable command line tool for Charm.

Usage
-----

Run the command on the command line to get usage information:

```
# ./vendor/bin/charm
```
