<?php
declare(strict_types=1);

namespace Charm\Cli;

class Term
{
    /**
     * Get the number of columns for the viewport.
     */
    public static function getCols(): int
    {
        return min(75, (int) (exec('tput cols') ?? 75));
    }
}
