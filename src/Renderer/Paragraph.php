<?php
declare(strict_types=1);

namespace Charm\Cli\Renderer;

class Paragraph extends AbstractRenderer
{
    protected $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function __toString()
    {
        return wordwrap(rtrim($this->text)."\n\n", $this->getColumns());
    }
}
