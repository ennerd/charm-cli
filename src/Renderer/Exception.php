<?php
declare(strict_types=1);

namespace Charm\Cli\Renderer;

use Throwable;

class Exception extends AbstractRenderer
{
    protected $error;

    public function __construct(Throwable $error)
    {
        $this->error = $error;
    }

    public function __toString()
    {
        $string = \get_class($this->error).' (code='.$this->error->getCode().")\n";
        $string .= new Paragraph($this->error->getMessage());

        return $string;
    }
}
