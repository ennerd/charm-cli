<?php
declare(strict_types=1);

namespace Charm\Cli\Renderer;

use Charm\Cli\Term;

abstract class AbstractRenderer
{
    protected function getColumns(): int
    {
        return Term::getCols();
    }

    abstract public function __toString();
}
