<?php
declare(strict_types=1);

namespace Charm\Cli;

abstract class AbstractCommand
{
    /**
     * Return the base command which would pass control to this command, when
     * running `charm <base-command>`.
     */
    abstract public function getBaseCommand(): string;

    /**
     * Describe this command in a few words.
     */
    abstract public function getSummary(): string;
}
