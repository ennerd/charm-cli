<?php
declare(strict_types=1);

namespace Charm;

use Throwable;

if (file_exists($path = __DIR__.'/vendor/autoload.php')) {
    require $path;
} elseif (file_exists($path = \dirname(__DIR__, 3).'/vendor/autoload.php')) {
    require $path;
} else {
    echo "run `composer install` first\n";
    exit(2);
}

try {
    $hooks = Hooks::instance();

    $listeners = $hooks->dispatch('Charm\Cli commands');

    if (0 === \count($listeners)) {
        throw new Error('No commands integrated. The charm command should be used together with an application using the Charm framework.', 1);
    }
} catch (Throwable $e) {
    echo new Cli\Renderer\Exception($e);
}
